package com.edutec.listviewfebrero.beans;

public class Hamburger {

    // Atributos
    private String nombre;
    private String precio;
    private int image;

    // Constructor vacio
    public Hamburger() {
    }

    // Sobrecarga de constructor
    // Constructor con parametros
    public Hamburger(String nombre, String precio, int image) {
        this.nombre = nombre;
        this.precio = precio;
        this.image = image;
    }

    // Getters and setters

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
