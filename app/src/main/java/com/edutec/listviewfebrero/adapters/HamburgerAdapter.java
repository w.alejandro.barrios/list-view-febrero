package com.edutec.listviewfebrero.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.edutec.listviewfebrero.R;
import com.edutec.listviewfebrero.beans.Hamburger;

import java.util.ArrayList;

public class HamburgerAdapter extends ArrayAdapter<Hamburger> {

    public HamburgerAdapter(Activity context, ArrayList<Hamburger> listItems){
        super(context, 0, listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView = convertView;

        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.item_hamburger, parent, false);
        }

        // Buscar la hamburguesa actual
        Hamburger currentHamburger = getItem(position);

        // Conectar con la vista inflada
        TextView itemName = listItemView.findViewById(R.id.itemNombre);
        TextView itemPrice = listItemView.findViewById(R.id.itemPrecio);
        ImageView itemImage = listItemView.findViewById(R.id.itemImage);

        // Cargar a la vista inflada los datos de la haburguesa actual
        itemName.setText(currentHamburger.getNombre());
        itemPrice.setText(currentHamburger.getPrecio());
        itemImage.setImageResource(currentHamburger.getImage());

        return listItemView;
    }
}
