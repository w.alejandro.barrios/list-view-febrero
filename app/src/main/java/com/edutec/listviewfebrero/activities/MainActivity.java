package com.edutec.listviewfebrero.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.edutec.listviewfebrero.R;
import com.edutec.listviewfebrero.adapters.HamburgerAdapter;
import com.edutec.listviewfebrero.beans.Hamburger;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lvHamburger = findViewById(R.id.lvHamburguesa);

        final ArrayList<Hamburger> hamburguesas = new ArrayList<Hamburger>();
        Hamburger quesoburguesa = new Hamburger();
        quesoburguesa.setNombre("Quesoburguesa");
        quesoburguesa.setPrecio("Q25.00");
        quesoburguesa.setImage(R.drawable.hamburger);

        Hamburger torito = new Hamburger("Torito", "Q20.00", R.drawable.hamburger);

        hamburguesas.add(quesoburguesa);
        hamburguesas.add(torito);
        hamburguesas.add(new Hamburger("Steak", "Q30.00", R.drawable.hamburger));

        final HamburgerAdapter hamburgerAdapter = new HamburgerAdapter(this, hamburguesas);

        lvHamburger.setAdapter(hamburgerAdapter);

        lvHamburger.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),
                        "Clickeaste: " + hamburguesas.get(position).getNombre(),
                        Toast.LENGTH_LONG).show();

            }
        });

        lvHamburger.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),
                        "Eliminar: " + hamburguesas.get(position).getNombre(),
                        Toast.LENGTH_LONG).show();
                hamburguesas.remove(position);
                hamburgerAdapter.notifyDataSetChanged();
                return true;
            }
        });



    }
}
